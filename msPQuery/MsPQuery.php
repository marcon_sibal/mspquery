<?php

namespace msPQuery;

use msPQuery\HTMLUtilities\HTMLUtilities;
use msPQuery\HTMLUtilities\Indentions;
use msPQuery\HTMLUtilities\Attributes;
use msPQuery\HTMLUtilities\Contents;
use msPQuery\HTMLUtilities\HTMLInterface;

/**
 * Creates HTML codes.
 */
class MsPQuery extends HTMLUtilities  implements HTMLInterface
{
    public function __construct()
    {
        $this->indentions = new Indentions;
        $this->attributes = new Attributes;
        $this->contents =  new Contents;
    }
}
