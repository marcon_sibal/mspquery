<?php

namespace msPQuery\HTMLUtilities;

/**
 * Content methods store contents in an array and
 * return HTML string base on what stored.
 *
 * 2017(c) Marcon Sibal
 */
class Contents
{
    /**
     * A cache for HTML content.
     * 
     * @var array
     */
    private $content = [];
    
    /**
     * A cache for the generated HTML string.
     * 
     * @var string 
     */
    private $contentString = "";
    
    /**
     * Replaces the content.
     * 
     * @param type $content
     */
    
    public function content($content)
    {
        if (!empty($content)){
            $this->content = [$content];
            $this->processChild($content);
        }
    }
    
    /**
     * Appends at the end  of the current content.
     * 
     * @param type $content
     */
    public function append($content)
    {
        if (!empty($content)){
            $this->content[] = $content;
            $this->processChild($content);
        }
    }
    
    /**
     * Appends at the beginning of the current content.
     * 
     * @param type $content
     */
    public function prepend($content)
    {   
        if (!empty($content)){
            array_unshift($this->content, $content);
            $this->processChild($content);
        }
    }
    
    /**
     * Returns the merge HTML String.
     * 
     * @return type
     */
    public function createContentString()
    {
        $content_string = "";

        if (count($this->content) > 0){
            
            foreach ($this->content as $content){
                
                if (is_object($content)){
                    $content_string .= $content->output(false);
                } else {
                    $content_string .= $content;
                }
            }
            
            $this->contentString = $content_string;
        }
        
        return $content_string;
    }
    
    /**
     * Returns the content array.
     * 
     * @return type
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Registers the parent of the HTML tag.
     * 
     * @param \MarconSibal\HTML\HTMLUtilities\HTMLInterface $child
     */
    private function processChild($child)
    {
        if (is_object($child)){
            if ($child instanceof HTMLInterface){
                $child->registerParent($this);
            }
        }
    }
    
    /**
     * Sets the content on a specific index.
     * 
     * @param int $index
     * @param string $content
     */
    public function setContent(int $index = 0, $content)
    {   
        if (isset($this->content[$index])){
            $this->content[$index] = $content;
        }
    }
}