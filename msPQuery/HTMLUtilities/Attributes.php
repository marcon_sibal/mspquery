<?php

namespace msPQuery\HTMLUtilities;

/**
 * Attributes methods store HTML attributes and generate
 * HTML attributes string.
 *
 * 2017(c) Marcon Sibal
 */
class Attributes
{
    /**
     * Create attribute for HTML tag.
     * 
     * @var array
     */
    private $attrCache = [];
    
    /**
     * Inserts attributes to the attrCach property.
     * 
     * @param type $attr
     * @param string $value
     */
    public function attr($attr, string $value = "")
    {
        if (is_array($attr)){
            foreach($attr as $attribute => $attr_value){
                $this->addAttribute($attribute, $attr_value);
            } 
        } else {
            $this->addAttribute($attr,$value);
        }
    }
    
    /**
     * Inserts an attribute to the attrCache property.
     * 
     * @param type $attr
     * @param string $value
     * @return $this
     */
    private function addAttribute(string $attr, string $value = "")
    {
        // if key value pair
        if (!empty($value)){
            // if attribute is not class.
            if (strtolower($attr) !== "class"){
                $this->attrCache[$attr] = $value;
            } else {
                $this->attrCache[$attr] = [$value];
            }
        } else {
            $this->attrCache[] = $attr;
        }
    }
    
    /**
     * Converts key-pair or just the key to HTML attribute string.
     * 
     * @param string $attr
     * @param string $value
     * @return $attr_string
     */
    private function attributeToString(string $attr, string $value = null)
    {
        $attr_string = "";
        
        if (!empty($attr)){
            $attr_string .= " {$attr}";
        }
        
        if (!empty($value)){
            $attr_string .= "=\"{$value}\"";
        }
        
        return $attr_string;
    }
    
    /**
     * Converts array to HTML attributes string.
     * 
     * @param array $attributes
     * @return $attr_string
     */
    public function attributesToString()
    {
        if (count($this->attrCache) == 0){
            return;
        }
        
        $attr_string = "";
        
        foreach($this->attrCache as $attribute => $value){
            // if not associative array
            if (is_int($attribute)){
                $attribute = $value;
                $value = null;
            }
            
            if (strtolower($attribute) === "class"){
                $value = $this->classArrayToString($value);
            }
            
            $attr_string .= $this->attributeToString($attribute, $value);
        }

        return $attr_string;
    }
    
    /**
     * Retrieve or set the id attribute.
     * 
     * @param string $id
     * @return string 
     */
    public function id(string $id = "")
    {
        if (empty($id)){
            return $this->getIDFromAttributes();
        } else {
            $this->attrCache["id"] = $id;
            return $id;
        }
    } 
    
    /**
     * Retrieves ID from attributes.
     * 
     * @return string
     */
    private function getIDFromAttributes()
    {   
        $id = false;

        foreach ($this->attrCache as $attr => $value){   
            
            if (strtolower($attr) ==  'id'){
                $id = $value;
                break;
            }
        }
        return $id;
    }
    
    /**
     * Adds class to the class attributes.
     * 
     * @param string $class_name
     */
    public function addClass(string $class_name = "")
    {
        if (!empty($class_name)){
            if (!isset($this->attrCache["class"])){
                
                $this->attrCache["class"] = [$class_name];
            } else {
                // add in the class array
                $this->attrCache["class"][] = $class_name;
            }
        }
    }
    
    /**
     * Returns the imploded string of classes.
     * 
     * @param array $class_array
     * @return string
     */
    private function classArrayToString(array $class_array)
    {
        $class_string = implode(" ", $class_array);
        
        return trim($class_string);
    }
    
    /**
     * Removes specific class from the class attribute.
     * 
     * @param string $class_name
     */
    public function removeClass(string $class_name = "")
    {   
        if (!empty($class_name)){
            if (isset($this->attrCache["class"])){
                // look for the class.
                $class_index = array_search($class_name, $this->attrCache["class"]);
                // if class exist unset class.
                if (!is_null($class_index)){
                    unset($this->attrCache["class"][$class_index]);
                }
            }
        }
    }
}