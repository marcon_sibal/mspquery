<?php

namespace msPQuery\HTMLUtilities;

use msPQuery\HTMLUtilities\Indentions as Indentions;
use msPQuery\HTMLUtilities\Attributes as Attributes;
use msPQuery\HTMLUtilities\Contents as Contents;
use msPQuery\HTMLUtilities\HTMLInterface;

/**
 * HTMLUtilities class is a helper class for generating
 * HTML codes.
 * 
 * 2017(c) Marcon Sibal
 */
class HTMLUtilities implements HTMLInterface
{     
    /**
     * A cache for the HTML tag.
     * 
     * @var string 
     */
    private $tag;
    
    /**
     * A flag to start the HTML tag on a new line.
     * 
     * @var bool 
     */
    private $hasNewline = false;
    
    /**
     *A flag to start the HTML close tag on a new line.
     * 
     * @var bool
     */
    protected $hasContentTab = false;
    
    /**
     * The parent HTML tag of this HTML tag.
     * 
     * @var bool 
     */
    protected $parent;
        
    /**
     * A cache for the Content class instance.
     * 
     * @var string 
     */
    protected $contents;
    
    /**
     * A cache for the Indentions class instance.
     * 
     * @var class
     */
    protected $indentions;
    
    /**
     * A cache for the Attributes instance.
     * 
     * @var class
     */
    protected $attributes;
    
    public function __construct(Indentions $indentions, Attributes $attr, Contents $contents)
    {
        $this->indentions = $indentions;
        $this->attributes = $attr;
        $this->contents = $contents;       
    }
    
    /**
     * Creates a new instance and sets its attr property.
     * 
     * @param string $tag
     * @return $html_tag new self
     */
    public function createTag(string $tag)
    {
        $html_tag = new self(new Indentions, new Attributes, new Contents);
        $html_tag->tag = strtolower($tag);
        
        return $html_tag;
    }
    
    /**
     * Check if the tag is self closing.
     * 
     * @param string $tag
     * @return type
     */
    private function isSelfClosingTag(string $tag)
    {
        $self_closing_tags = [
            "area", "base", "br", "col", "command", "embed", "hr", "img",
            "input", "keygen", "link", "menuitem", "meta", "param", "source",
            "track", "wbr"
        ];
        
        return in_array($tag, $self_closing_tags);
    }
    
    /**
     * Start the opening HTML tag with a newline.
     * 
     * @param type $has_newline
     * @return $this
     */
    public function newline(bool $has_newline = true)
    {
        $this->hasNewline = $has_newline;
        
        return $this;
    }
    
    /**
     * Start the closing HTML tag on a newline.
     * 
     * @param bool $has_content_tab
     * @return $this
     */
    public function contentTab(bool $has_content_tab = true)
    {
        $this->hasContentTab = $has_content_tab;
        
        return $this;
    }    
  
    /**
     * Generates the HTML output.
     * 
     * @param bool $is_outermost
     * @return string
     */
    public function output(bool $is_outermost = true)
    {   
        if ($is_outermost){
            $this->indentions->indentContent($this);
        }
        
        $settings = $this->getSettings("hasNewline");
        $attr = $this->attributesToString(); 
        $tag_ending = ($this->isSelfClosingTag($this->tag))? " />" : ">";
        
        $HTML_output = $settings["newline"] . $settings["indention"]
                . "<{$this->tag}{$attr}{$tag_ending}";
        
        if (!$this->isSelfClosingTag($this->tag)){
            $content = $this->contents->createContentString();
            
            if(!empty($content)){
                $HTML_output .= $content;
            }
            
            $settings = $this->getSettings("hasContentTab");
            
            $HTML_output .= $settings["newline"] . $settings["indention"]
                    . "</{$this->tag}>";
        }
        
        return $HTML_output;
    }
    
    /**
     * Returns newline and tab definitions.
     * 
     * @param string $property
     * @return array
     */
    public function getSettings(string $property)
    {
        $settings = [
            "newline" => "",
            "indention" => "",
            "tab" => ""
        ];
        
        if ($this->$property){
            $settings["newline"] = NEWLINE;
            $settings["indention"] = $this->indentions->getIndention();
            $settings["tab"] = TAB;
        }
        
        return $settings;
    }
    
    /**
     * Registers the parent HTML tag.
     * 
     * @param HTMLInterface $parent
     */
    public function registerParent($parent)
    {
        $this->parent = $parent;
    }
    
    /*******************************
     * Content related methods
     *******************************/
    
    /**
     * Replaces the content.
     * 
     * @param string $content
     * @param bool $has_newline
     * @return $this
     */
    public function content($content)
    {
        $this->contents->content($content);
        
        return $this;
    }
    
    /**
     * Appends to the end of the current content.
     * 
     * @param type $content
     * @return $this
     */
    public function append($content)
    {
        $this->contents->append($content);
        
        return $this;
    }
    
    /**
     * Appends to the beginning of the content.
     * 
     * @param type $content
     * @return $this
     */
    public function prepend($content)
    {
        $this->contents->prepend($content);
        
        return $this;
    }
    
    /**
     * Returns the content array.
     * 
     * @return array
     */
    public function getContent()
    {
        return $this->contents->getContent();
    }
    
    /**
     * Sets the content on a specific index
     * 
     * @param int $index
     * @param type $content
     */
    public function setContent(int $index = 0, $content)
    {
        $this->contents->setContent($index, $content);
    }
    
    /*******************************
     * Attributes related methods
     *******************************/
    
    /**
     * A short-hand to store attributes
     * 
     * @param type $attr
     * @param string $value
     * @return $this
     */
    public function attr($attr, string $value = "")
    {
        $this->attributes->attr($attr, $value);
        
        return $this;
    }
    
    /**
     * A shorthand that returns a constructed HTML attributes string.
     * 
     * @return string
     */
    private function attributesToString()
    {
        return $this->attributes->attributesToString();
    }
    
    /**
     * Retrieve or set the id attribute.
     * 
     * @param string $id
     * @return string
     */
    public function id(string $id = "")
    {
        return $this->attributes->id($id);
    }
    
    /**
     * Add class to the html_tag.
     * 
     * @param string $class_name
     */
    public function addClass(string $class_name = "")
    {
        $this->attributes->addClass($class_name);
    }
    
    /**
     * Removes specific class from the class attribute
     * 
     * @param string $class_name
     */
    public function removeClass(string $class_name = "")
    {
        $this->attributes->removeClass($class_name);
    }
    
    /*******************************
     * Indention related methods
     *******************************/
    
    /**
     * Use this to adjusts tab indention of HTML tag.
     * 
     * @param int $tab_total
     * @return $this
     */
    public function indent(int $tab_total = 0)
    {
        $this->indentions->indent($tab_total);
        
        return $this;
    }
    
    /**
     * Returns the generated tab indention for this tag.
     * 
     * @return type
     */
    public function getIndention()
    {
        return $this->indentions->getIndention();
    }
}