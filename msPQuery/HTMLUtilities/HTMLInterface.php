<?php

namespace msPQuery\HTMLUtilities;

interface HTMLInterface
{
    public function output(bool $is_outermost = true);
}