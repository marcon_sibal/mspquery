<?php

/**
 * Configuration Settings and constant definition.
 *
 * 2017(c) Marcon Sibal
 */

/**
 * Tab string used for indention.
 */
define("TAB", "\t");

/**
 * Newline string for code line breaks.
 */
define("NEWLINE", "\r\n");