<?php

use msPQuery\MsPQuery;

if (file_exists("vendor/autoload.php")){
    // Use composer autoload if existing
    require_once "vendor/autoload.php";
} else {    
    //Load if not using autoloader
    require_once "msPQuery/HTMLUtilities/Attributes.php";
    require_once "msPQuery/HTMLUtilities/Indentions.php";
    require_once "msPQuery/HTMLUtilities/Contents.php";
    require_once "msPQuery/HTMLUtilities/HTMLInterface.php";
    require_once "msPQuery/HTMLUtilities/HTMLUtilities.php";
    require_once "msPQuery/MsPQuery.php";
}


if (!function_exists("msPQuery")){
    function msPQuery(string $tag)
    {   
        $msPQuery = new MsPQuery;
        return $msPQuery->createTag($tag);
    }
}
